Rails.application.routes.draw do
  resources :comments
  # Player routing
  devise_for :players, :controllers => { registrations: 'registrations' } # Use custom registrations controller for custom devise fields
  # Game routing
  resources :games, param: :slug do
    get :autocomplete_tag_name, :on => :collection
  end

  # Tag routing
  resources :tags, only: [:index, :show]

  # Difficulty routing
  resources :difficulty, param: :name

  # Types routing
  resources :types, param: :name

  # Contact routing
  resources :contacts, only: [:new, :create]

  # Users routing
  get "/players/:username", to: "players#show", as: "show_player"

  resource :players do
    resource :avatar do
      match '/delete_avatar', to: 'players#delete_avatar', via: 'delete'
    end
  end

  # Pages routing
  get "/contact" => "contacts#new"
  get "/difficulties" => "difficulty#index"
  get "/:page" => "pages#show"

  # Set root path
  root :to => "pages#show", :page => 'home'
end
