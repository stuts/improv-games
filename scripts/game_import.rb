player = Player.find_by username: 'stuts'
PaperTrail.request.whodunnit = "1"
#game = player.games.new(:name => 'A World Without...', \
#        :name_alt => '', \
#        :difficulty_id => '1', \
#        :type_id => '1', \
#        :players_min => '2', \
#        :players_max => '99', \
#        :description => 'Players do short scenes or one-liners about a world without whatever the audience suggests. ', \
#        :intro => '', \
#        :notes => '', \
#        :sample => '', \
#        :tag_list => 'warmup, fill in the blank, a to c, free assocation' \
#        )
#game.save!

#game = player.games.new(:name => 'Battle Scene Galactica', \
#        :name_alt => '', \
#        :difficulty_id => '1', \
#        :type_id => '2', \
#        :players_min => '4', \
#        :players_max => '6', \
#        :description => 'Players split into two teams. One team leaves the room. The other team gets a suggestion from the audience for a scene, and then performs it. Then the first team returns and, without knowing what the other team did, does a scene based on the exact same suggestion. The audience is then asked to show which team they liked better by applause.', \
#        :intro => '', \
#        :notes => '', \
#        :sample => '', \
#        :tag_list => 'audience, ' \
#        )
#game.save!

#game = player.games.new(:name => 'Blind Stage Direction', \
#        :name_alt => '', \
#        :difficulty_id => '1', \
#        :type_id => '2', \
#        :players_min => '2', \
#        :players_max => '2', \
#        :description => 'Before the game, the audience gives stage directions. The player then pulls these out from a slip of paper and must perform what is on the paper. Played like Whose Line.', \
#        :intro => '', \
#        :notes => 'A variation could be that one person is given 3 lines and the other is given 3 stage directions, or they are mixed up. ', \
#        :sample => '', \
#        :tag_list => 'justification, ' \
#        )
#game.save!

#game = player.games.new(:name => 'Evil Twin', \
#        :name_alt => '', \
#        :difficulty_id => '1', \
#        :type_id => '2', \
#        :players_min => '4', \
#        :players_max => '4', \
#        :description => 'Two players act out a scene based on an audience suggestion. However, each of the players has an "evil twin". At any time the host can switch out one of the players with his or her evil twin. When they switch back, the good twin has to try to justify why he or she acted evilly.', \
#        :intro => '', \
#        :notes => '', \
#        :sample => '', \
#        :tag_list => 'switch, ' \
#        )
#game.save!

#game = player.games.new(:name => 'Innuendo/If you know what I mean', \
#        :name_alt => '', \
#        :difficulty_id => '1', \
#        :type_id => '2', \
#        :players_min => '2', \
#        :players_max => '2', \
#        :description => 'A scene is played where as many lines as possible are turned into innuendo by saying, "If you know what I mean" at the end of the line of dialog. ', \
#        :intro => '', \
#        :notes => '', \
#        :sample => '', \
#        :tag_list => 'verbal restriction, ' \
#        )
#game.save!

#game = player.games.new(:name => 'Look What I Found', \
#        :name_alt => '', \
#        :difficulty_id => '1', \
#        :type_id => '2', \
#        :players_min => '2', \
#        :players_max => '3', \
#        :description => 'The audience is asked to give suggestions of objects, and the objects are written onto slips of paper. The players do a straight scene, but during the scene pull out and read the strips of paper and must then introduce that object into the scene and justify it.', \
#        :intro => '', \
#        :notes => '', \
#        :sample => '', \
#        :tag_list => 'justification, ' \
#        )
#game.save!

#game = player.games.new(:name => 'Sex is like/Sex with _____ is like', \
#        :name_alt => '', \
#        :difficulty_id => '1', \
#        :type_id => '2', \
#        :players_min => '2', \
#        :players_max => '99', \
#        :description => 'Players are given a suggestion and then must explain how sex or sex with soemone is like that suggestion. ', \
#        :intro => '', \
#        :notes => '', \
#        :sample => '', \
#        :tag_list => 'fill in the blank, ' \
#        )
#game.save!

#game = player.games.new(:name => 'That\'s What She Said', \
#        :name_alt => '', \
#        :difficulty_id => '1', \
#        :type_id => '2', \
#        :players_min => '3', \
#        :players_max => '3', \
#        :description => 'The players split into teams. One team starts doing a scene until another team shouts, "That\'s what she said!" and then that team starts a brand new scene using the last line from the last scene as the first line of their scene.', \
#        :intro => '', \
#        :notes => '', \
#        :sample => '', \
#        :tag_list => 'switch, justification, endowment, ' \
#        )
#game.save!

game = player.games.new(:name => 'Alliteration', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '3', \
        :description => 'The audience provides a letter (either to all players or to individual players). Players must then complete the scene using as many words as possible that start with that letter. Could also be just one player. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'verbal restriction, ' \
        )
game.save!

game = player.games.new(:name => 'Blind Dubbing', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '4', \
        :description => 'Two players act out a scene, while two other players provide their dialogue. However, the dubbers are not able to see the scene, and therefore the actors have to fill-in everything the dubbers say.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'verbal restriction, physical restriction, ' \
        )
game.save!

game = player.games.new(:name => 'Distillation', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'The players act out a scene based on the audience\'s suggestion. They then replay the scene, reducing the amount of dialogue, thus distilling the scene. This is repeated until the scene can be distilled no more.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'verbal restriction, ' \
        )
game.save!

game = player.games.new(:name => 'English/Gibberish ', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'During a scene, the host will ding the bell, causing the players to switch to gibberish, and back to English each time the bell rings', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'swtich, verbal restriction, ' \
        )
game.save!

game = player.games.new(:name => 'Fortune Cookie Challenge', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '4', \
        :description => 'Each player is given a fortune cookie, chosen by the audience. They read their fortune, then must use that fortune to influence their character. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'endowment, narrative, ' \
        )
game.save!

game = player.games.new(:name => 'Forward/Reverse', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'A scene plays out, and during the scene, the host can rewind or fast forward the scene. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'jump, physical, ' \
        )
game.save!

game = player.games.new(:name => 'Genre House', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '3', \
        :description => 'Three spaces are marked on stage via chairs/stools. Each zone represents a different genre (or emotion, etc). Every time the players move into the space, they must assume that space\'s characteristics. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'transformation, ' \
        )
game.save!

game = player.games.new(:name => 'Genre Transfer', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Players begin the scene with one genre, and throughout the scene they must gradually shift to the other genre. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'transformation, ' \
        )
game.save!

game = player.games.new(:name => 'Identity Crisis', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '3', \
        :players_max => '3', \
        :description => 'The players start doing a straight scene based on the audience\'s suggestion. Each player should have a distinct character that isn\'t like the other characters in the scene. At any time the host can call out "Switch" and all the players must switch characters and then resume the scene. One way to do this is to have each of the characters wear something distinct and when the host calls "switch," everyone throws the item on the floor and picks up a new item and that determines which character they are now.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'switch, ' \
        )
game.save!

game = player.games.new(:name => 'Inverse Party Quirks', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '99', \
        :description => 'Three players leave the room. The audience then gives suggestions of celebrities, quirks, etc. to each player out of the room. The audience then gives a reason the host is throwing a party. The guests come in one at a time and the party host must then get each player to guess their identity given by the audience.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'naive, endowment, ' \
        )
game.save!

game = player.games.new(:name => 'Missing Letter', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '3', \
        :players_max => '3', \
        :description => 'Two players do a scene but cannot say any words containing the letter given by the audience. If any player says a word with that letter, hesitates too long, or doesn\'t make sense, they are eliminated and replaced by another player.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'verbal restriction, ' \
        )
game.save!

game = player.games.new(:name => 'Night Gallery', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '3', \
        :players_max => '7', \
        :description => 'Three titles of paintings are given by the audience. A player (as a Rod Serling-esque host) introduces one "painting" at a time, vaguely describing the tale behind it. The "painting" is two other players frozen in place center stage. After the player acting as host finishes describing their tale, the players begin the scene starting from their positions in the painting. The host ends the scene. This is done three times, each painting made by a different pair of players.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'endowment, narration, ' \
        )
game.save!

game = player.games.new(:name => 'Parallel Universe', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'The scene will start with the first suggestion and at some point the host will call "switch" during the scene. The players will then need to switch to the a new scene based on the other suggestion and justify their positions. The host can then switch back and forth as often as he likes, with the players justifying their positions each time.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'switch, ' \
        )
game.save!

game = player.games.new(:name => 'Adjectives', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '3', \
        :description => 'The host gets a list of adjectives from the audience. During the scene, the host will yell freeze, then say one of the adjectives. The scene continues with this adjective influencing the players and scene. (Variation of 10 Emotions)', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'list, narrative, ' \
        )
game.save!

game = player.games.new(:name => 'Blind, Deaf, and Dumb', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '3', \
        :players_max => '3', \
        :description => 'Each of the three players are limited by one sense, with a blindfold and headphones, and the third cannot speak. They must then work together to complete the scene. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'restriction, ensemble' \
        )
game.save!

game = player.games.new(:name => 'Double-Speak', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '4', \
        :description => 'One player provides all the dialog of the other characters in the scene, including his own. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'verbal restriction, ' \
        )
game.save!

game = player.games.new(:name => 'Euphemism', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Players are given a commong saying like, "a rolling stone gathers no moss." One player acts as the narrator/director and introduces players and characters, slowly working toward an ending that would justify the saying, and ending with that saying. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'objective, narrative, endowment, side coaching, ' \
        )
game.save!

game = player.games.new(:name => 'Human Props', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '4', \
        :description => 'Two players do a scene based on an audience suggestion and the other two players act as all their props throughout the entire scene.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'physical restriction, physical, ' \
        )
game.save!

game = player.games.new(:name => 'Jeopardy', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '4', \
        :description => 'The naive player assumes the role of Alex Trebec. The other three players are assigned the roles of a living person, a dead person, and an imaginary person. Trebec will ask the audience for category suggestions and writes them down, taking care to not make them too specific. Play continues as it would on Jeopardy with Trebec starting by generally introducing the show and the contestents as "contestants" and a general introductory question like, "To what charity will you be donating your winnings?"  Play continues until Trebec can guess the contestants. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'naive, endowment, ' \
        )
game.save!

game = player.games.new(:name => 'Letter Switch', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'A common consonant is replaced with another consonant given by the audience. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'verbal restriction, ' \
        )
game.save!

game = player.games.new(:name => 'Memento', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'A play is performed by the players in reverse order of Act 3, Act 2, then Act 1. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'jump, ' \
        )
game.save!

game = player.games.new(:name => 'Missing Person', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '4', \
        :description => 'One player will sit out a scene which is based on the audience\'s suggestion. But the other three players will act as if player four is there and react to an "invisible person". Then the scene is redone with player four filling in as the person missing from the scene. He or she must fit in to the blocking and dialogue of the scene.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'naive, justification,?, ' \
        )
game.save!

game = player.games.new(:name => 'Multiple Personalities', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '3', \
        :players_max => '3', \
        :description => 'An item will be gathered from the audience for each player and a scene will be suggested. Then each item will be given a character, mannerism, or celebrity. The players must then incorporate the items into the scene, and whoever holds the item must become the persona that is attached to it.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'endowment, switch, ' \
        )
game.save!

game = player.games.new(:name => 'Transformation', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '99', \
        :description => 'Two players begin a scene with a physical activity designated by the audience. "Through a series of physical heightening and extensions and transformations between them, they will evolve into other activities. And, then eventually other people will join them in those activities, They\'ll finally end up with the whole ensemble working together through all these transformations in a group activity" chosen by the audience at the start of the game. ', \
        :intro => '', \
        :notes => '***This game would only be played if all participants seem to be doing well with the previous games.***', \
        :sample => '', \
        :tag_list => 'transformation, ' \
        )
game.save!

game = player.games.new(:name => 'Upstage, Downstage', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '4', \
        :description => 'Two players begin a scene downstage, often sitting to provide a good view of the scene behind them. While they are doing their scene, the others will play out a silent scene, one that somehow relates to the first scene, but take place in a different environment Both scenes should play out without pausing, but giving and taking focus. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'switch, pantomime, environment, ' \
        )
game.save!

game = player.games.new(:name => 'ABC', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '3', \
        :description => 'Players are given a starting letter. The player\'s sentence must start with that letter. The next player\'s sentence starts with the next letter, and so on. See if players can reach all the way around withing a minute. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'verbal restriction, ' \
        )
game.save!

game = player.games.new(:name => 'Blind Freeze', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '99', \
        :description => 'Players stand on the back line with their backs turned to the audience. Two players are chosen to begin a scene. At any time during the scene, a player on the back line may yell out "Freeze!" The two players in the scene will freeze in their position. The player who yelled freeze will then take one of the player\'s spots, assuming the exact position of the player, then starting a brand new scene with that posititon as their inspiration. ', \
        :intro => '', \
        :notes => 'Sometimes it is best to create moments where a freeze can naturally occur. For example. a player saying, "Okay, here it comes, in 3, 2, 1!" In other words, providing clues and hints to the back line when you are doing something physical. ', \
        :sample => '', \
        :tag_list => 'justification, physical, justification, physicality, ensemble' \
        )
game.save!

game = player.games.new(:name => 'Entrances and Exits', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '2', \
        :players_min => '3', \
        :players_max => '3', \
        :description => 'Each player is given a word. When they hear that word, they must justify their exit/entrance. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'justification, ' \
        )
game.save!

game = player.games.new(:name => 'Fill in the Blank', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Two players utilize two audience members to provide words, gestures, and sounds when needed. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'audience, ' \
        )
game.save!

game = player.games.new(:name => 'New Choice', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Players perform a scene. The Moderator can then ring the bell and say, "New Choice." The player must then repeat their offer with a different choice. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'justification, ' \
        )
game.save!

game = player.games.new(:name => 'Numbers', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '2', \
        :players_min => '3', \
        :players_max => '3', \
        :description => 'Players are given a number from the audience, 1-9. The player may/must use that assigned number of words each time they speak. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'verbal restriction, ' \
        )
game.save!

game = player.games.new(:name => 'The Brain', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '2', \
        :players_min => '3', \
        :players_max => '3', \
        :description => 'The players are asked questions by the audience and must answer one word at a time. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'verbal restriction, audience, ' \
        )
game.save!

game = player.games.new(:name => 'Whose Line', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Players pull strips of paper from a bowl that have phrases submitted by the audience. The players must justify the phrases in the scene. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'justification, media, ' \
        )
game.save!

game = player.games.new(:name => 'Yes/No', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'An audience member is given a piece of paper with either Yes or No responses on them, and can only use those as their dialog. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'audience, ' \
        )
game.save!

game = player.games.new(:name => '10 Emotions', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Moderator gets 10 emotions or states of mind from audience. Freezes scene and interjects those emotions. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'list, ' \
        )
game.save!

game = player.games.new(:name => 'Action Figures ', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Players are moved by audience members during the scene. Can only talk. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'other bodies, audience, physical restriction, ' \
        )
game.save!

game = player.games.new(:name => 'Bullshit', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Two players perform a scene, but they can call bullshit on something their partner said, who must then tell the truth. Variation of New Choice. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'justification, , ' \
        )
game.save!

game = player.games.new(:name => 'Catchphrase', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '3', \
        :players_max => '3', \
        :description => 'This game is played with three people. Two of the people have "catch phrases", two lines of dialogue each (one question and one statement) that are collected from the audience. During the scene those "catch phrases" are the only thing they can say. They may change the inflection or add different punctuation, but they may not change the words or the order of them. The third person must play the scene like normal and help bring it to a conclusion.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'verbal restriction, ' \
        )
game.save!

game = player.games.new(:name => 'Charlie', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '3', \
        :players_max => '4', \
        :description => 'Offstage player is endowed with three characteristics by the onstage players. Charlie comes in and enacts characteristics. Players then enquire about this unusual quirks. Charlie explains, then justifies an exit. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'endowment, justification, ' \
        )
game.save!

game = player.games.new(:name => 'Circle of Friends', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '3', \
        :players_max => '3', \
        :description => 'Two players begin a scene with a third in the wings. The two players then talk about the third player and endow him with an unusual quirk. The player enters the scene with the quirk (usually triggered in some way). One of the first to players finds a reason to exit, then the two remaining players endow them with a quirk. The player returns, and then the third player leaves. Eventually, all three will be on stage with their quirks hopefully all reacting together in a big mess until they can find an natural end to the scene. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'endowment, ' \
        )
game.save!

game = player.games.new(:name => 'Clue ', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '99', \
        :description => 'Host and/or onstage player is given Who, Murder Weapon, and Location by the audience. Offstage player then comes on stage. Onstage player can only speak in gibberish and pantomime to try to convey these three things. When Offstage player gets it, she murders OP, then the process repeats until all players have gone, then reveal what they think the answers were. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'physical, ' \
        )
game.save!

game = player.games.new(:name => 'Dubbing', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '4', \
        :description => 'Two onstage players\' dialog is given by two offstage players. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'verbal restriction, ' \
        )
game.save!

game = player.games.new(:name => 'Emotion Switch', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Two players begin a scene, one or both players are given a starting emotion and an ending emotion. Throughout the course of the scene, they must gradually shift from one to the other, using context of the scene to justify the change. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'transformation, ' \
        )
game.save!

game = player.games.new(:name => 'Emotional Trade', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Players are each endowed with a different emotion. During the scene, they must slowly switch their emotional state to that of the other player. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'transformation, ' \
        )
game.save!

game = player.games.new(:name => 'Foreign Film Dub', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '4', \
        :description => 'Players speak in gibberish accents, and are then "dubbed" by offstage players. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'verbal restriction, ' \
        )
game.save!

game = player.games.new(:name => 'He Said, She Said', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'During the scene, players provide the action the other player must peform, then their own dialog. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'psyche, endowment, ' \
        )
game.save!

game = player.games.new(:name => 'Interrogation', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '32', \
        :players_max => '99', \
        :description => 'One naive player is interrogated by two other players. Via pun and innuendo, the naive player must guess what innocuous crime they are guilty of, where they committed this crime, and with whom, all of which are supplied by the audience. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'naive, ' \
        )
game.save!

game = player.games.new(:name => 'Job Interview', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Naive player is interviewing for an unsual job derived from the audience. Interviewer assist the player by asking leading questions and using clues, puns, and innuendo. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'naive, ' \
        )
game.save!

game = player.games.new(:name => 'Late to Work', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '4', \
        :description => 'In this scene there is one guesser, two clue givers, and the boss. The guesser has to guess how he was trying to get to work, what stopped him, and how he overcame the obstacle. The clue givers may only do charades, and must help the guesser without getting the attention of the boss.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'naive, pantomime, ' \
        )
game.save!

game = player.games.new(:name => 'Party Quirks', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '99', \
        :description => 'A naive player is hosting a party chosen by the audience. Guest begin to arrive who are endowed with quirky characteristics. The host must then try to guess the quirks, removing guests as they do so. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'naive, endowment, ' \
        )
game.save!

game = player.games.new(:name => 'Performance Art', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'During a scene, the host will freeze the scene then ask for a movie genre, style of music, favorite actor/singer/writer, etc. Players will then peform the scene based on this suggestion until the next freeze. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'list, narrative, ' \
        )
game.save!

game = player.games.new(:name => 'Props ', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '4', \
        :description => 'Two sets of players are given a prop (doesn\'t have to be the same prop) and must come up with as many ways to use the prop not related to its actual use as possible, alternating from team to team. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'endowment, justification, ' \
        )
game.save!

game = player.games.new(:name => 'Returns Counter', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'A naive player is returning an unusual object chosen by the audience. The player at the counter must get the NP to guess the object through obscure clues and pun. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'naive, ' \
        )
game.save!

#game = player.games.new(:name => 'Scenes From a Hat', \
#        :name_alt => '', \
#        :difficulty_id => '2', \
#        :type_id => '2', \
#        :players_min => '3', \
#        :players_max => '99', \
#        :description => 'Players are given scene suggestions submitted by the audience from a hat/bowl. ', \
#        :intro => '', \
#        :notes => '', \
#        :sample => '', \
#        :tag_list => 'fill in the blank, ' \
#        )
#game.save!

game = player.games.new(:name => 'Super Heroes', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '3', \
        :players_max => '99', \
        :description => 'The first player is endowed with an unusual super power by the audience and given an innocuous world problem to solve. As the player sets the scene, other players will enter the scene. When the first one enters, she will endow them with an unusual super power. Then that player will endow the next player to enter. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'endowment, ' \
        )
game.save!

game = player.games.new(:name => 'Text Me Later', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'A player is given a cell phone from the audience. That player may only use text messages as their dialog during the scene. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'verbal restriction, media, audience, ' \
        )
game.save!

game = player.games.new(:name => 'Things you can say about _____, but not your _____', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '99', \
        :description => 'Players are given a suggestion, and then must create puns or innuendo about the suggestion. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'fill in the blank, ' \
        )
game.save!

game = player.games.new(:name => 'Two-Faced Date', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '3', \
        :players_max => '3', \
        :description => 'One player is on a date with the other two players. The two players are polar opposites, and represent two sides of the same person. When the host rings the bell, the players switch chairs and the scene continues. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'switch, ' \
        )
game.save!

game = player.games.new(:name => 'Two-Headed Monster', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '3', \
        :players_max => '4', \
        :description => 'Two players act as a single character, speaking in unison for the scene. ', \
        :intro => '', \
        :notes => 'Sometimes it is best if one player leads most of time and the other adds comments here and there. But, it depends on how comfortable players are with one another. ', \
        :sample => '', \
        :tag_list => 'verbal restriction, ' \
        )
game.save!

game = player.games.new(:name => 'Word on Back', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'A word is taped to each player\'s back. They must get the other player to say their word without making it too obvious. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'naive, verbal restriction, ' \
        )
game.save!

game = player.games.new(:name => 'World\'s Worst', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '3', \
        :players_max => '99', \
        :description => 'Players are given a world\'s worst situation, then players come up with as many short scenarios based on that suggestion as possible. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'fill in the blank, ' \
        )
game.save!

game = player.games.new(:name => '3 Rooms', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '6', \
        :players_max => '6', \
        :description => 'Players are paired into individual scenes (rooms). Moderator rings the bell to switch scenes in order. The last line of the last scene is the first line in the next scene. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'switch, justification, ' \
        )
game.save!

game = player.games.new(:name => 'Back in My Day', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '99', \
        :description => 'Two players play elderly people who are reminiscing about their youth. They say, "back in my day" and follow it with increasingly ridiculous claims while the other players act the claims out. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'endowment,   pantomime, ' \
        )
game.save!

#game = player.games.new(:name => 'Scene Roulette', \
#        :name_alt => '', \
#        :difficulty_id => '3', \
#        :type_id => '2', \
#        :players_min => '4', \
#        :players_max => '4', \
#        :description => 'Players are arranged in a "box/rectangle." Each side is an invidual scene. Moderator switches scenes, speeding up as the game progresses. ', \
#        :intro => '', \
#        :notes => '', \
#        :sample => '', \
#        :tag_list => 'justification, narrative, switch, ' \
#        )
#game.save!

game = player.games.new(:name => 'Dating Game', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '4', \
        :description => 'Naive player/audience member will question three players in Dating Game style. NP must guess their quirks after 2-3 rounds. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'endowment, naive, ' \
        )
game.save!

game = player.games.new(:name => 'Director', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '3', \
        :players_max => '99', \
        :description => 'One player acts as the "director" of a new movie, beginning the scene by telling the characters about the movie, then directing them what to do. He then can cut the scen and reshoot it as he sees fit. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'endowment, side-coach, ' \
        )
game.save!

game = player.games.new(:name => 'Eugene', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '4', \
        :description => 'Long form improv that uses an audience suggestion but does not have an opening. Scenes are generated from the suggestion and then are gradually tied together by the end if possible. Sometimes employs the "Slacker Method" of scene changes. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'long form, ' \
        )
game.save!

game = player.games.new(:name => 'Fairy Tale News', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '4', \
        :description => 'A news host, with a special guest, talk about a Fairy Tale news event, and go to an on scene reporter who has eye-witnesses. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'endowment, justification, ' \
        )
game.save!

game = player.games.new(:name => 'Film Critics', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '99', \
        :description => 'Two players review a fictional film derived from the audience. As they discuss three of the scenes, other players act out those vignettes. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'endowment, justification, ' \
        )
game.save!

game = player.games.new(:name => 'Panel of Experts', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '3', \
        :players_max => '99', \
        :description => 'Each player is a subject expert. Their specialty can be given by the audience, or a subset of their expertise. They then answer audience questions through that lens. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'endowment, character, ' \
        )
game.save!

game = player.games.new(:name => 'Character Carousel', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '4', \
        :description => 'Players are placed in a square. The front of the square represents a scene. As the scene progresses, the host will rotate the square left and right, and the players assuming those spots must assume the character for that spot. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'switch, narrative, ' \
        )
game.save!

game = player.games.new(:name => 'Sit, Stand, Lean', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '3', \
        :players_max => '3', \
        :description => 'At any time, a player must be standing, sitting, or leaning. If one player changes, other players must assume that posititon. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'physical, justification, ' \
        )
game.save!

game = player.games.new(:name => 'Slide Show', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '99', \
        :description => 'Two players begin to talk about their vacation destination given by the audience. Either the other players can assume positions and the 2 can justify, or they provide a general scene and the players assume those positions and further justification is given. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'physical restriction, justification, ' \
        )
game.save!

game = player.games.new(:name => 'Soliloquy', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '3', \
        :description => 'Players step out toward the audience during a scene while the other players soft freeze. The player then gives his inner thoughts or feelings, then step back into the scene. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'psyche, narrative, ' \
        )
game.save!

game = player.games.new(:name => 'Helping Hands', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '3', \
        :description => 'Another player or audience member stands behind the player and puts their arms through the players arms, which then go around their back. The player\'s/audience member\'s arms are then the players arms for the scene. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'other bodies, ' \
        )
game.save!

game = player.games.new(:name => 'Alien, Tiger, Cow', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Everyone closes their eyes. On the count of three. On three, each player will choose either "Alien," "Tiger," or "Cow." Each has a specific gesture associated. The idea is to encourage a group mind. Play continues until all players choose the same thing. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Arnold Talking about Cyrptids', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'In a circle, everyone chooses either the same or their own cryptid (vampire, sasquatch, werewolf, etc). When the moderator says "go," everyone will do an impromptu monoloque about their cryptid using their best Arnold impression, all at the same time. ', \
        :intro => '', \
        :notes => 'Requires intense focus and self-listening skills. ', \
        :sample => '', \
        :tag_list => 'warmup, listening, focus' \
        )
game.save!

game = player.games.new(:name => 'Catch This', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players stand in a circle. A player will initiate by saying someone\'s name, and asking them to catch something. It doesn\'t matter what the offer is, the recipient must catch it as accurately or creatively as they can. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Chameleon', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players stand in a back line. One player is chosen to begin. Other players then step up to the first player and initiate a strong character choice. The first player must then mimic the characterization. The scene is short, maybe a few lines. This continues three times, then the last one to initiate becomes the chameleon. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Character Walk', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players walk around the space as the moderator gives them emotional and character traits for them to enact. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, emotion, character' \
        )
game.save!

game = player.games.new(:name => 'Clap, Snap, Word Association', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players sit in a circle and begin the pattern: pat legs, clap, snap right, snap left. The first player starts with a random word. The next player (clockwise) must then say that word on the snap right and then word associates on snap left. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Communal Monologue', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'The audience gives the title of a story that has never been written. One player begins telling the story as a character. At any time, another player from the back line may tag out the speaker and continue exactly where the story left off, speaking as the same character.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Copy Cat', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'A player stands in the middle of the cirlce and closes their eyes. The moderator secretly chooses one player to start a movement that the others will copy. The player in the middle then opens their eyes. They watch the players. When the player who was secretly chosen changes movements, the others copy, trying to do it in a way not to give away who is deciding the movements. The player in the middle must then guess who the secret player was. If they are right, the secret player then goes in the middle. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Cross the Room', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players stand on one side of the room. The moderator will choose someone and say, "Cross the room as ______." The player then embodies that choice, giving it 100%. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Dail it to 11', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Game is played similar to 10 emotions, but with a "volume" knob for the emotions, allowing players to explore not only a range of emotions, but also to explore the possible magnitudes of those emotions in more depth. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Defender/Assassin', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players stand scattered around the room. The moderator tells them to randomly choose someone to be their assasin, and one person to be their defender (silently). When the moderator says, "Go," players will try to keep the defender between them and thier assassin. Play continues for serveral minutes. At the end, players try to guess who each player was. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Draw', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players stand in a circle. One player stands in the middle and turns, and will randomly point at a player and say, "Draw!" The player that is chosen must duck. Then, the players on either side of the chosen player must draw and shoot the other. The loser then stands in the middle, taking the other player\'s place. If it is a draw, the players point their guns, take a deep breath, and say "Bang, bang, bang..." until one of the them runs out of breath. The loser stands in the center of the circle. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Ear Plug Scenes', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'An improv scene but with ear plugs or fingers in your ears, or headphones. ', \
        :intro => '', \
        :notes => 'When it\'s hard to hear someone it forces you to really make an effort to listen to what they\'re saying. This is also a good exercise for people that talk to quietly. It forces them to talk louder just to be heard. (Think Fast Improv)', \
        :sample => '', \
        :tag_list => 'warmup, listening' \
        )
game.save!

game = player.games.new(:name => 'Emotion Circle', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Around a circle, every player just has an emotional reaction. They don\'t need words – they can just make an emotional sound. Have them go around and then go around pushing their emotions to 11.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, emotion' \
        )
game.save!

game = player.games.new(:name => 'Emotional Context', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players form two "lay-up" style lines on either side of the stage. Players at the front of each line decide on an emotion inside their heads. Player from the stage left line comes out and says "I [blank] you" (i.e. "I love you"). Player from the stage right line comes out and says "I know" filtered through the emotion they chose ahead of time (i.e. they chose "sad" so they say "I know" very depressed). Have both player repeat their lines 3 or 4 times, heightening their emotions each time.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, emotion' \
        )
game.save!

game = player.games.new(:name => 'Flocking', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'A grown-up version of "Follow the Leader," this exercise is great to initiate movement in improvisation. Like "Follow the Leader," one player is in charge of creating the movement and the others must follow. Rather than forming a line, however, the dancers are grouped together like a flock of birds. That way, when the current leader turns in any direction, the player now in front of the group becomes the group leader. This is a great activity because the players must learn to work while moving in a closely established group and also learn to think quickly when they are leading others.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, movement' \
        )
game.save!

game = player.games.new(:name => 'Friends and Family', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => '"Everyone think about someone you know really well. Think about their tics. How do they talk? How do they stand? What do they do with their hands?    "One at a time, in no particular order, each of you is going to enter the circle and – for 30 seconds – act and speak as the friend or family member you\'re thinking about. You don\'t need to have this person talk about themselves; I don\'t need to know their name, their relationship to you, etc.  Just inhabit them for 30 seconds. It can help to imagine what their doing right now while we\'re doing this and do that. At the end of 30 seconds, I\'ll clap, that person will exit the circle\'s center and the next player will enter it."', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Gimme 5', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players stand in a circle. Starting player turns to the player on their left and says, "______, gimme 5 _____," and then asks for a list of any five things. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Have It, Take It, Leave It', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'In a circle, players can say "Have It" to pass it to the side, "Take It" to go across the circle, or "Leave It" to bounce it back to the sender. They can also say, "You\'re not me Mum" and they have to change places with the sender. Or, they can say, "Get out of me pub" and everyone changes places. All of this is done in a terrible Cockney accent. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Heightening Emotional Agreement', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'A player makes a Self Contained Emotional Statement. It can be as simple as "I love it here," "I hate the arts," or "I\'m uncomfortable." Then progressively each person to the right heightens the perspective by agreeing with it – essentially with a "Yes, and." "I love the beach." "Yeah, I love the white sand." "Yeah, I love getting my tan on." Etc. The initiator gets the final addition. And then the person to their right starts a new SCES.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, yes and, emotion, heightening' \
        )
game.save!

game = player.games.new(:name => 'Inspired by an Object', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players will choose random object (use inanimate at first). They will then perform a free-walk with a character/movement/style/etc. inspired by that object. Then, try the same exercise with an animal as inspiration. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, movement, character' \
        )
game.save!

game = player.games.new(:name => 'Interrupting Scenes', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Do a regular improv scene, but you have to start speaking before your scene partner finishes his or her thought.', \
        :intro => '', \
        :notes => 'Getting constantly interrupted doesn\'t feel like being listened to. The key to this improv exercise is digesting what your scene partner is saying and recognizing when they\'ve reached the main point or thesis of their whole spiel. That\'s the moment to tell them to shut up and start talking about whatever you want to talk about. (Think Fast Improv)', \
        :sample => '', \
        :tag_list => 'warmup, listening, ensemble' \
        )
game.save!

game = player.games.new(:name => 'Is This Your Card?', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '8', \
        :description => 'Players are given a card from a normal playing card deck. Without looking at the card, they will hold it up to their head and either begin walking around the room or perform scenes. Based on the rank of their card, that will determine their character\'s status. Other players must react accordingly. To change it up, flip the ranks in the middle. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, status' \
        )
game.save!

game = player.games.new(:name => 'Last Letter, First Letter', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '99', \
        :description => 'In either pairs or in a circle, the first player will offer a statement. The last letter of their last word will be the first letter of the next player\'s word, and so on. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, listening, storytelling' \
        )
game.save!

game = player.games.new(:name => 'Last Line, First Line', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Two players, or a circle of players start with a single line. The last word of that line must then be used by the next player as the first word of their line. This continues until the scene finds a natural ending. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Mirror Game', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players pair up. One player is designated as the initiator, and the other player is the mirror image. The player mirroring the initiatiator must mimic every move the other player makes, including facial expressions. The goal here is not to see if the player can trip up the mirroring player, but rather for the two players to work together in order to make it appear as a seamless mirror image. ', \
        :intro => '', \
        :notes => 'A social skills building game. The intent is to create an empathic connection through physical mimicry, allowing the player to see themselves as other people both metaphorically and literally if the participants can match the moves as exactly as possible. ', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Monologue Hotspot', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'A player begins a monologue. At any time during the monologue, another player may tag the other player out, and continue the monologue, usually taking the last thing they said and turning the monologue for comedic effect. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Name Thumper', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Going around the circle, each person (teacher included) associates their name with an action or adjective – "Punching Patrick," or "Pouting Patrick." Go around once more so everyone knows everyone else\'s name and action. Then play progresses with an individual doing their name/action and then another person\'s name/action; that person then does their name/action and then another person\'s name/action; etc.', \
        :intro => '', \
        :notes => 'A great ice-breaker that not only helps moderators learn the names of the participants, but helps new improvisers learn that it is okay to be silly. Also creates a sense of comradery. ', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'New Rhyme', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Played like New Choice, but new line must rhyme with the previous line. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Not Funny', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '99', \
        :description => 'Players must play the scene as seriously as possible. If a player laughs, they are eliminated, and replaced. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'One Word/Line Story', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players stand in a line or circle. One word at a time, players will create a story and try to end it logically. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Orchestra', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players stand in a line. A "conductor" will then point at a player who will then start a repetitive motion and sound. This continues until all players are moving and making sounds. The conductor can then lower and raise the volume, or silence certain "insturments." ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Pass the Clap', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players stand in a circle. The moderator turns to the person on the left, makes eye contact, and they both clap at the same time, or keep clapping until they do. That player then passes the clap to the person on their left. The key is not to create an easy rhytm throughout the circle, but ot treat each instance as a seperate moment. The players must maintain eye contact, and try to avoid signaling their partner. ', \
        :intro => '', \
        :notes => 'This is a teamwork and ensemble game. ', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Pass Yes Around', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'A player points at / makes eye contact with another player who accepts by saying "Yes." The accepted player walks across the circle to stand in the place of the player who said "Yes." The player who said "Yes" points at / makes eye contact with another player who says "Yes" so they can exchange physical position. And repeat. ', \
        :intro => '', \
        :notes => 'By emphasizing "Yes," and utilizing eye-contact, this game solidifies basic improv techniques vital to learning more advanced techniques. ', \
        :sample => '', \
        :tag_list => ', ' \
        )
game.save!

game = player.games.new(:name => 'Point of View Switch', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '1', \
        :players_max => '2', \
        :description => 'Like Emotion Switch, players will begin the scene  with a strong point of view ("I hate pizza/Pizza is life," "Dogs are better/Cats are better," etc.), and must slowly change to the other player\'s POV by the end of the scene. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Questions Only', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Two players begin a scene, but can only use questions. When they fail to do so, they are replaced by the next player. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => ', ' \
        )
game.save!

#game = player.games.new(:name => 'Red Ball, Blue Ball', \
#        :name_alt => '', \
#        :difficulty_id => '1', \
#        :type_id => '2', \
#        :players_min => '4', \
#        :players_max => '8', \
#        :description => 'Players stand in a circle. Leader tosses a "red ball" to another player. Player catches it saying, "Thank you red ball," then throws it to someone else. As this continues, the leader will then throw out the yellow and blue ball. At some point, the leader will halt the game and ask who has which balls. If more than one ball of any color is being held, someone wasn\'t listening!', \
#        :intro => '', \
#        :notes => '', \
#        :sample => '', \
#        :tag_list => ', ' \
#        )
#game.save!

game = player.games.new(:name => 'Red Ball, Red Bull, Bread Bowl', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '8', \
        :description => ' With the group in a circle, a player starts by saying, "Dustin, Red Ball" then mimes throwing to that player who catches it, says "Red Ball, Thank you" then passes it by saying "Lauren, Red Ball." Then you add more pretend balls/objects and try and keep them all going.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => ', ' \
        )
game.save!

game = player.games.new(:name => 'Rumor', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players stand in a circle. One player turns to his left and asks that player if they\'ve heard a rumour. The other player replies that they have but wonders if they\'ve also heard this twist on the rumor. They then turn to the next player with a new rumor. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => ', ' \
        )
game.save!

game = player.games.new(:name => 'Self-Contained Emotional Statement', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Around a circle, everyone makes a Self Contained Emotional Statement.  It can be as simple as "I love it here," "I hate the arts," or "I\'m uncomfortable."  Lessons:  - It\'s a statement. Not a question shifting the responsibility of providing information to your partner. There\'s a period. It\'s definitive.  - It\'s an emotional statement. Emotional reaction is one of our three key tools; let\'s get to it. You need to feel and, for the reaction, you need to give that feeling a direction. Give X the power to make you feel Y.  - Being self-contained, the statement places you on solid ground without dictating the scene to your partners – Mick Napier urges us each to "take care of yourself" without confining the scene. Allow your partner the choice of whether to mirror you in some fashion or to take on something entirely their own. Being self-contained is increasingly an imperative the larger a group you have on stage.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, emotion' \
        )
game.save!

game = player.games.new(:name => 'Set the Stage', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Initial player will enter an imagined space and interact with the space in some way, all without sound. The next player will then interact with the same object/action briefly in some way before adding their own interaction somewhere else in the space. By the end, all players should be able to guess the location based on the object work. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => ', ' \
        )
game.save!

game = player.games.new(:name => 'Show Me How You Get Down', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players stand in a circle. One person begins by selecting another player and saying, "Hey ____." The selected player then says, "Hey what." The 1st player then says, "Show me how you get down." They say, "No way." The 1st player says again, "Show me how you get down." Then the 2nd player says, "Okay." They then say, "D - O - W - N, this is how I get down," while doing a dance move of their choice. The second time they do that, all the players in the cirlce join in.  ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => '0, ' \
        )
game.save!

game = player.games.new(:name => 'Silent Scenes', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Do an improv scene without dialogue. Use your physicality and actions to show your emotions and communicate to your scene partner and the audience what\'s going on. Don\'t mime speaking silently, show it in your face and body.', \
        :intro => '', \
        :notes => 'This is a great exercise for getting in so in tune with your scene partner that you\'re getting the meaning of every little thing they do. Later, when you\'re actually using words at each other, you\'ll be that much more attentive. (Think Fast Improv)', \
        :sample => '', \
        :tag_list => 'warmup, ensemble' \
        )
game.save!

game = player.games.new(:name => 'Slowing Things Down', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Movement in slow motion is by far my favorite thing to do (as my students will definitely confirm). The fluid, controlled motion is mesmerizing to me and takes strength as well as focus.  For this exercise I choose a starting point and give the players a period of time (sometimes half an hour or more) to traverse the perimeter of the room. The leisurely pace allows for the exploration of a myriad of levels and shapes. As an added bonus, slow motion is a great cool down and is easy on the joints.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, movement' \
        )
game.save!

game = player.games.new(:name => 'So I\'ll...', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'This exercise forces listening and gets players taking smaller, more logical steps with their story building. It also helps players when they draw a blank in a performance setting. The first player makes any kind of statement. For instance, "It is a lovely day out." The next player in the line says, "WHAT YOU ARE SAYING IS THAT--It is a lovely day out, SO I WILL--go for a walk." The goal is to say the next most logical thing in the story. The next player would say "WHAT YOU ARE SAYING IS THAT--I\'ll go for a walk, SO I WILL--get my shoes." The story that builds should be a logical one. It will not be a story that will win Pulitzer prizes, but it will make sense.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

#game = player.games.new(:name => 'Song Spot', \
#        :name_alt => '', \
#        :difficulty_id => '1', \
#        :type_id => '1', \
#        :players_min => '4', \
#        :players_max => '8', \
#        :description => '', \
#        :intro => '', \
#        :notes => '', \
#        :sample => '', \
#        :tag_list => 'warmup, ' \
#        )
#game.save!

#game = player.games.new(:name => 'Sound Ball', \
#        :name_alt => '', \
#        :difficulty_id => '1', \
#        :type_id => '1', \
#        :players_min => '4', \
#        :players_max => '8', \
#        :description => '', \
#        :intro => '', \
#        :notes => '', \
#        :sample => '', \
#        :tag_list => 'warmup, ' \
#        )
#game.save!

game = player.games.new(:name => 'Stage-Coach', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players sit in chairs in a circle. Each player will designate a part of the stage-coach they will represent. The moderator/first player then also chooses a part, and begins telling a story about the stagecoach. Each time a player\'s part is mentioned, they must stand and turn in a circle and sit back down. If the word "stagecoach is said, then everyone must change seats. Whoever is left standing without a chair then continues the story from where it left off. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => ', ' \
        )
game.save!

game = player.games.new(:name => 'Start As, End As', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Similar to Cross the Room As, but the player starts in one state and ends in another, taking their time to make the transition. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => ', ' \
        )
game.save!

game = player.games.new(:name => 'Story Stealing', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '2', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Everyone in a circle. One at a time, players enter the center and tell a true, personal, 30 Second Story. Once everyone has told a story, the teacher tells the class that players now have to enter the center and recreate someone else\'s story. Every story should be revisited once by another player.', \
        :intro => '', \
        :notes => 'Lessons: - Don\'t mock; mirror – this is not about making fun of each other, it\'s about making each other look good by remembering their story - The more you remember, the more options you have – you might not get the chance to revisit the story you remember best so you need to work to remember everything - Remember specifically – remembering a few specific details will be more powerful than remembering everything generally - Remember reactions – our emotional reactions are improv gold; focus on those when setting other player\'s stories to memory - See what\'s not shown – recreating what our fellow players initially did subconsciously is great fun. How do they stand? How do they move? What do they sound like?', \
        :sample => '', \
        :tag_list => ', ' \
        )
game.save!

game = player.games.new(:name => 'Surprise Movement', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Have everyone walk around leisurely. Tel them to stop and start make a movement, a gesture, a sound, anything really. Let the players repeat the gesture until they know what they `are`.  The idea is not to preconceive, but to let it happen. Players may turn out to be dish washers, ushers, lawn mowers, ping pong balls, whatever. Explain that there are no wrong answers.    Tell the players just to acknowledge what they `are` for themselves, and then move on, stop again and make another gesture/movement/sound.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, movement' \
        )
game.save!

game = player.games.new(:name => 'Text and Talk', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Do an improv scene while texting someone else about something totally unrelated. If you don\'t have anyone to text, just read through your favorite Facebook or Twitter feed.', \
        :intro => '', \
        :notes => 'This one makes you better at multitasking and multitasking is pretty much the best thing ever.', \
        :sample => '', \
        :tag_list => 'warmup, listening, focus' \
        )
game.save!

game = player.games.new(:name => 'That Reminds Me', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Player is given a single word. They then say, "That reminds me," and then they tell an anecdote about that word. Then, the next player uses that story to insipire their anecdote, saying, "That reminds me," and so on. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, association' \
        )
game.save!

game = player.games.new(:name => 'That\'s Awesome', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players go around the circle saying something about their day. Everyone responds with a resounding, "That\'s Awesome!"', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'TJj & Dave', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Players begin a 1-3 minute scene with no prompt or suggestion, just using their scene partner as inspiration. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'What are you doing?', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'One person stands in the middle of a circle so everyone can see and begins pantomiming an action. Another comes up to the first and gently asks "What are you doing?" The first person continues doing their activity until they come up with something to say that is anything but the activity they\'re doing. If I was pantomiming starting a lawnmower, for example, I might say "solving a Rubik\'s cube." The second person then takes on that activity and the first person rejoins the outer circle. Soon thereafter, a third person comes in to ask "What are you doing?" and the game continues.', \
        :intro => '', \
        :notes => 'Make sure the questioner asks with honest and kind curiosity rather than with dismissal or sarcasm. It also helps to use the person\'s name: "Hey, Jan, what are you doing?" Try to be realistic with the actions rather than cartoonish. How would you actually do that activity? Again, encourage kids not to plan ahead but to let the new idea emerge from the confusion of the action they\'re doing. You may need to kibosh some suggestions intended to embarrass the next student (i.e. "I\'m masturbating," or "I\'m taking a dump.") Remind them that part of the reason you\'re playing these games is to learn how to take care of each other.', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'What\'s in the Box?', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => '2 players sit with a mimed box, and take turns asking each other what is in the box. They can either volley the question back and forth rapidly, or they can explore the object, taking their time, ', \
        :intro => '', \
        :notes => 'IRC 2010-03-10', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

#game = player.games.new(:name => 'Word Association', \
#        :name_alt => '', \
#        :difficulty_id => '1', \
#        :type_id => '1', \
#        :players_min => '4', \
#        :players_max => '8', \
#        :description => '', \
#        :intro => '', \
#        :notes => '', \
#        :sample => '', \
#        :tag_list => 'warmup, ' \
#        )
#game.save!

game = player.games.new(:name => 'Word Disassociation', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players walk around the room, point at an object, and call it something other than it is. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, association' \
        )
game.save!

game = player.games.new(:name => 'Yes Let\'s...', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players stand in a circle. One player will initiate an action, saying, "Let\'s ____," then mime the action. The other players will say, "Yes!" and then also mime the action. This continues briefly until someone else makes an initiation. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Yes, And', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Everyone stands in a circle. A player starts a story: "Billy loved his turtle." Starting with the player to the initiator\'s left, the group builds the story sentence by sentence, literally saying "Yes, and…" to begin each contribution: "Yes, and Billy and his turtle did everything together."', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

#game = player.games.new(:name => 'Yes, Because', \
#        :name_alt => '', \
#        :difficulty_id => '1', \
#        :type_id => '1', \
#        :players_min => '4', \
#        :players_max => '8', \
#        :description => '', \
#        :intro => '', \
#        :notes => '', \
#        :sample => '', \
#        :tag_list => 'warmup, ' \
#        )
#game.save!

game = player.games.new(:name => 'Zero, One, Many', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Players begin a scene not being able to say any words. Then, they are able to say one word, then many. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, listening' \
        )
game.save!

#game = player.games.new(:name => 'Zip, Zap, Zop (Whoosh, Bang, Pow)', \
#        :name_alt => '', \
#        :difficulty_id => '1', \
#        :type_id => '1', \
#        :players_min => '4', \
#        :players_max => '8', \
#        :description => '', \
#        :intro => '', \
#        :notes => '', \
#        :sample => '', \
#        :tag_list => 'warmup, ' \
#        )
#game.save!

game = player.games.new(:name => 'Blind Lead/Trust', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '99', \
        :description => 'One player is blindfolded and given an objective (e.g., find a chair and sit in it, or pick up a piece of paper and throw it in the garbage), then the rest of the group (or a chosen individual) will then guide the player through obstacles to complete the objective. ', \
        :intro => '', \
        :notes => 'Giving one\'s self over completely to the will of another person or goup can be a very stressful ordeal. But, when there is trust in a group, the experience can enlightening and strengthen team bonds. (Variation: Time limit, and player can only move forward, though they can turn in any direction, also known as Airplane)', \
        :sample => '', \
        :tag_list => 'ensemble, trust, team' \
        )
game.save!

game = player.games.new(:name => 'Character Interrogation', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Two players. One is the interviewer and asks leading questions of the other players, requiring them to justify the answers with "Yes, and" as their fundamental philosophy. ', \
        :intro => '', \
        :notes => 'People and Chairs', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Count to 10/20', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players stand in a circle, facing out, shoulder to shoulder. Without planning or using a regular pattern, players must count to 10/20. If players speak at the same time, they must start over. ', \
        :intro => '', \
        :notes => 'The emphasis of this exercise is teamwork and mindfulness. It is nearly impossible to complete this task without really focusing in on what is happening, and trying to tap into the group mind that happens as they work together to reach their goal. ', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Do What You Do Where You Do It', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Have a player engage in a mimed activity they are very familiar with in a space imagined based on their actual house/work/etc. Moderated by the instructor, players from the audience get to ask questions that the player has to respond to in mime ("What else is around you?" / "Is it hard to do or easy?" / "Do you like it or do you hate it?") – we want to drive students toward specifics.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'I Am a Tree', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'One person starts the scene on stage saying "I am a tree." Another person joins them, choosing something or someone to interact with the tree. They might say "I am the blue jay calling from the tree branch" and clasp the tree person\'s arm. Or maybe they say "I am the water running beneath the roots of the tree" and lie down on the floor to wriggle beneath the tree person\'s feet. A third person then joins the first two, choosing their own related identity and action: "I am the lovers\' carving in the bark on the tree" while forming a heart on the tree person\'s torso. At that point, the person who started the scene—here, the tree—chooses one of the others to take with her off stage ("I\'ll take the lovers\' carving.") and they leave the third person alone on stage. That person repeats their identity ("I am the blue jay calling from the tree branch.") and two more come on stage to find connected identities to that person. This person who was left on stage alone—this second time, the jay—now chooses one of his or her own to come off stage and the cycle begins again. Repeat as needed.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'In Both Ears', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '1', \
        :players_min => '3', \
        :players_max => '3', \
        :description => 'One player sits with two other players on either side of them. They both tell a story, or speak about something at the same time. The middle player must then try to re-tell both stories as accurately as they can. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, listening, focus' \
        )
game.save!

game = player.games.new(:name => 'Janus Dance', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Two players put their heads on each other\'s shoulders, facing away from one another, and then begin a small dance in unison. They must then complete a short scene. ', \
        :intro => '', \
        :notes => 'This game helps players who are not yet comfortable with one another to overcome any awkwardness (or create it!)', \
        :sample => '', \
        :tag_list => 'warmup, trust, familiarity' \
        )
game.save!

game = player.games.new(:name => 'Layering', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'This warm-up takes Chameleon to another level. Players are put into a scene. One is the straight man and the other is endowed with a character trait (i.e., old man, russian atheleter, etc.). The scene begins. At some point, the scene is then frozen and another "layer" is added to the character. For example, the class may yell out "he is afraid of committment," or "she lacks the ability to feel compassion." The idea isn\'t to add things like a limp or a hunchback, but those are fine as well. The idea is to dig deeper into a character by adding nuanced layers, allowing unusual combinations for players to explore. ', \
        :intro => '', \
        :notes => 'cs', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Mime Me', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Put the participants into groups of two. Ask one of each pair to come and get an index card. Without showing it to their partner, they must get the other person (the mime) to do what is on the card. They cannot tell them what is on the card, instead, they must direct the person\'s actions until they are doing the action. In the last example, they might tell the person to shiver (it\'s cold in Alaska). They might tell them to cast their arm backwards or to sit down… You get the idea.', \
        :intro => '', \
        :notes => 'THE RULES:    -The mime cannot speak, they can only follow the instructions.  -The game is done when the mime appears to be doing exactly what is on the card.  At that point, the second person goes to get a new index card and the roles are reversed.  -The first person cannot touch the mime, or do the action for them.  They must stand still, giving direction with their words only.  -Builds trust within the pairs, creative thinking and listening skills. (User Friendly)', \
        :sample => '', \
        :tag_list => 'warmup, listening, ensemble' \
        )
game.save!

game = player.games.new(:name => 'Mind Meld', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players stand in a circle. Moderator will call out 2 players\' names. When the moderator counts to 3 the players will shout out the first word that comes to mind. These two words then act as the inspiration for the other players. When the words together make the players think of another associated word, they raise their hand. The moderator then calls on two more people and the process is repeated until two players say the same word. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'One Person Walks', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Students spread out through the room. Tell one person to start walking around the room, among the other students who remain frozen in space. Without talking – with one person walking at any given time – students take and give the power to walk. One person starts, the other stops; one person stops, the other starts. Students have to see each other to know when to give and take focus.', \
        :intro => '', \
        :notes => 'Varation--Two people walk. ', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Solitaire', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Players line up on both sides of a wide surface. Another player walks/runs from one end to the other with their eyes closed, the other players guiding them with their voice and keeping them from getting hurt. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, trust' \
        )
game.save!

game = player.games.new(:name => 'Totems', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '3', \
        :description => 'Players are given an animal to inspire them in their scene, but they don\'t become the animal. You can also have the other players guess the animal. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, character' \
        )
game.save!

game = player.games.new(:name => 'Tug of War', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Everybody has a tug of war but the rope is invisible, the rules are that the rope must look real, can\'t stretch or be elastic. Have a little miming moment: "Feel the rope" etc. We aren\'t playing by actual tug of war rules; the point is to have a scene where we look like we are. We aren\'t on opposing teams; we\'re all on the same "doesn\'t this look like a real tug of war?" team.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

#game = player.games.new(:name => 'Walks in a Bar', \
#        :name_alt => '', \
#        :difficulty_id => '2', \
#        :type_id => '1', \
#        :players_min => '4', \
#        :players_max => '8', \
#        :description => '', \
#        :intro => '', \
#        :notes => '', \
#        :sample => '', \
#        :tag_list => 'warmup, ' \
#        )
#game.save!

game = player.games.new(:name => 'Who Am I (Spolin)', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'A naive player is given an identity by the other players (start with something general, then try a specific person similar to Interview). Through interaction, the naive player will take on the characteristics of that endowment. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'You Can\'t Say That', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Players are both given a word that are not able to say. If they say the word the rest of the group yells, "You can\'t say that!" and they are out, and another player takes their place. Start with easy words, but choose a scene that would normally require them to say it, including any homophones. For example, the player can\'t say cat, but the scene takes place at a vet or pet store. The other players can then try to get them to say that word, while remembering not to say theirs. Eventually, work up to very common words such as "and." yo', \
        :intro => '', \
        :notes => 'This game will force players to learn to edit on the fly while doing a scene. This is helpful when doing shows that have a general audience or children and the troupe wants to avoid offending anyone. Being able to edit, while still accomplishing silencing the inner critic and A to C is an invaluable skill. ', \
        :sample => '', \
        :tag_list => 'warmup, editing' \
        )
game.save!

game = player.games.new(:name => 'A Loss for Words', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Scene is performed as a TJ and Dave, but with no words. this doesn\'t mean miming. The players will say the words in their heads and act them out with natural face and body reactions. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'verbal restriction, ensemble, ' \
        )
game.save!

game = player.games.new(:name => 'Sender/Receiver', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '2', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Two players stand across from each other. One is the sender, the other is the receiver. The sender tries to communicate their character, their relationship to their scene partner, their want or situation – all without miming or speaking. The receiver then says what they got from the other person&rsquo;s energy and body language.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => ', ' \
        )
game.save!

game = player.games.new(:name => 'TJ and Dave Guessing Game', \
        :name_alt => '', \
        :difficulty_id => '3', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Very specific scenes (I.e., "I\'m late for my first day as a third grade sub and the principal is upset, but doesn\'t know I was late because I was helping an old woman change a tire.") are written on index cards. The sender tries to convey as much of this as possible without explicitly telling the receiver. ', \
        :intro => '', \
        :notes => 'cs', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Patterns', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Get everyone in a standing circle, have everyone raise one hand and name a category that has at least several different answers, such as Australian animals or breakfast cereals. One person points to someone else in the circle who has their hand up and names something from that category and keeps their hand pointing at that person. The person who has just been pointed at, points to someone else and names something else from that category while keeping their hand pointed at this new person. This continues until the last person, who points at the person who started the pattern before naming one more thing from that category. This way, everyone has had a go at adding to the pattern and the random order will help prevent planning ahead. After finishing the pattern, repeat it a number of times in the same order it was first delivered.  .', \
        :intro => '', \
        :notes => 'After this, discuss as a group if any of the things named stood out from the rest of the pattern and why. This emulates the discovery of an unusual thing in a scene\'s base reality', \
        :sample => '', \
        :tag_list => 'warmup, , no editing, free assoc.' \
        )
game.save!

game = player.games.new(:name => 'Heighten and Explore', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Two players perform a scene. One player is Heighten, and they can only heighten the scene offers. The other player is Explore, and can only explore and expand on the offers. This will result in a very stiff and artificial scene, but will help players understand the necessity of both tactics, as well as how they play off one another. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup , heighten/explore' \
        )
game.save!

game = player.games.new(:name => 'Discovery vs. Invention', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '99', \
        :description => 'Moderator gives an initiation or first offer. Players on back line will then step forward with their reply, then indicate what they found in the initiation that informed their decision. Then discuss if that reply was discovered from information provided in the initial offer, or if the player invented the reply. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'Justify This', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Players perform a scene, and during the scene they will pull a slip of paper from a bowl, much like in Whose Line. But, instead of having phrases, the bolw will contain things that must be incorporated into the scene, such as a quirk, a location, a secret, a relationship, etc. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, justification' \
        )
game.save!

game = player.games.new(:name => 'Da Doo Run Run', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Sing the original song one line per person with improvised bits  Person 1 - "I met him on a Monday and his name was Bill  All - "Da doo ron ron ron, da doo ron ron"  Person 2 - "He looked very sick, he was very ill"  All - "Da doo ron ron ron, da doo ron ron"  All - "Yeah"  Person 3 - "He took a pill"  All - "Yeah"  Person 4 - "It worked until"  All - "Yeah"  Person 5 - "He grew some gills"  All - "Da doo ron ron ron, da doo ron ron.  Person 6 - "I met her on a Tuesday and her name was Anne"  etc.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'music, warmup, music' \
        )
game.save!

game = player.games.new(:name => 'Rhyme Ball', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Person 1 holds an imaginary ball They throw it to Person two and say a word. While "catching" the ball, Person 2 rhymes with the word While "throwing" the ball to Person 3 they associate with that word Continue. eg: Person 1 <throw> - "cat" Person 2 <catch> - "hat". <throw> - "wig" Person 3 <catch> - "big". <throw> - "huge" Person 4 <catch> - "luge". <throw> - "bob" etc.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, music, rhyming' \
        )
game.save!

game = player.games.new(:name => 'Hush Little Baby', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Sing Hush little baby improvising the words. eg:  Person 1 <think of something "Daddy" is going to buy you> - Daddy\'s going to buy you a piece of string  Person 2 <think of what could go wrong> - And if that piece of string\'s too short.  Person 2 <Rhyme with yourself - short> - Daddy\'s gonna buy you a bottle of port  Person 3 - And if that bottle of Port\'s gone off  Person 3 - Daddy\'s gonna buy you a picture of The Hoff  etc.  This is a great way of practicing associating and rhyming in a concise format', \
        :intro => '', \
        :notes => 'Backing Track ', \
        :sample => '', \
        :tag_list => 'warmup, music, music, rhyming' \
        )
game.save!

game = player.games.new(:name => 'Blues', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'There are many different ways of singing the blues in terms of structure but they all rely on the same principles.  This example is for a 12-bar blues.  Ask for an everyday problem or nuisance.  Sing a rhyming couplet relating to the problem  Sing "Oh yeah I got the blah blah blah Blues"  Alternatively you can sing the same line twice and then the rhyming line.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, music, music' \
        )
game.save!

game = player.games.new(:name => 'Favorite Things', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'All sing through the verse/chorus of "Favourite things" to familiarise players with the tune  Each line will be improvised by each player in turn  Player 1 thinks of a category and sings things in that category, eg Fruit  Player 1 - "Apples and lemons and strawberries and plums"  Player 2 recognises the category and sings more things, the last of which rhymes!  Player 2 - "Bananas and pears and peaches shaped like bums"  Player 3 Sets up the next player to rhyme with the category...  Player 3 - "Anyone can eat them, wimps or brutes"  Player 4 completes the rhyme...  Player 4 - "These are a few of my favourite fruits"  Player 5 sings something that goes wrong with that thing  Player 5 - "When the core rots"  Player 6 does the same  Player 6 - "When they\'re squishy"  Player 7 same (they could set up a bad word for Player 8 if they are super-charged)  Player 7 - "When they fall on your head"  Player 8 sings two lines and rhymes  Player 8 - "I simply remember my favourite fruits, and then I don\'t end up dead"  All congratulate themselves about whatever just happened ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'music, warmup, music, rhyming' \
        )
game.save!

game = player.games.new(:name => 'Set-Ups', \
        :name_alt => '', \
        :difficulty_id => '2', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'A category is given eg sports  Player 1 tries to set up Player 2 with a relevant rhyming word  Player 1 - "I hit balls through hoops, I hope that\'s okay"  Player 2 attempts to complete the rhyme  Player 2 - "Of course it is if you\'re playing croquet"  Player 2 tries to set up Player 3 with a relevant rhyming word"  Player 2 - "You cant play this without a wicket"  Player 3 has a zen-like moment of nothingness and cannot think of anything...  Player 3 - "I can\'t play that so please just stick it" etc.  Completing the rhyme successfully is not compulsory.  Better to relax and enjoy yourself.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'music, warmup, rhyming' \
        )
game.save!

game = player.games.new(:name => 'Royal Status Game', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'This is a Keith Johnstone classic in which a high-status king/queen is alone on stage and servants come and tend to him/her. If they annoy the Royal person in any way, he/she clicks their fingers and the servant dies. I love this exercise because it forces people to play boldly. Often people think they are playing low status but you can always go lower! And the same can be said of a high-status character. Also, this game encourages making a choice rather than being polite and importantly it\'s very, very funny to watch. ', \
        :intro => '', \
        :notes => 'Unsure how it looks in action. ', \
        :sample => '', \
        :tag_list => 'warmup, character, status' \
        )
game.save!

game = player.games.new(:name => 'Plak', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'A non-musical exercise. This is best played with a small group (6 max). We stand close together.  Our aim is to come up with a slogan that you might see on a fridge magnet or car window s  cker.  One person says a word: for example, "Badgers". Anyone can go next: "please". And then anyone  again: "apologise". If you think we\'ve completed our slogan (which clearly, we have, because  badgers clearly need to apologise), then we push our palms into the circle and say "Plak!".  Hopefully, we all say "Plak!" together because we\'re all building the same group mind. And then  we create more slogans.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, group mind' \
        )
game.save!

game = player.games.new(:name => 'What\'s Going On?', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'It\'s loosely based on the song "What\'s Up" by 4 Non-Blondes, which is better known by the yelled tagline "What\'s Going On?".  Again we stand in a circle. Someone chooses a location, for example "vampire castle". Each person  in turn sings a rhyming couplet that describes the environment that fits the melody, e.g.:  There\'s a statue before me dripping with blood  Don\'t know what it means but it can\'t be good  And the whole group sings:  I said hey—what\'s going on?  And we continue around the circle.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, musical, musical, rhyming, rhythm' \
        )
game.save!

game = player.games.new(:name => 'Go Big or Go Home', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Short organic scenes are performed, but players are encouraged by the "audience" to make big choices, big characters, high stakes. If they make a small choice and the audience boos, they must ramp it up until the audience cheers. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, confidence, team, character, stakes' \
        )
game.save!

game = player.games.new(:name => 'Mimic', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '99', \
        :description => 'Everyone stands in a circle and someone says something. It can be a movie quote, song lyric or something from real life. The next person in the circle observes the person saying the quote and then mimics exactly what they saw to the next person in circle. Over time it will change and get weird. They key is to copy the person next to you and not anyone previous.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, character, physical, listening' \
        )
game.save!

game = player.games.new(:name => 'You Look, You Seem', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'It crystallises the notion that The Scene Is In The Eyes Of Your Partner. You are constantly making endowments based on close observation of your scene partner, they in turn run with your endowments to generate more content for you, do this for a few minutes then switch.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, endowment, character, observation, listening, physical' \
        )
game.save!

game = player.games.new(:name => 'Volcano', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'Everyone walking around the room. The director shouts out something and counts to 5 and everyone has to physically form that thing with each other before the director gets to 5. For instance "Volcano, 1, 2, 3, 4, 5! Microwave over, 1, 2, 3, 4, 5!". Used to get everyone moving around and having fun and also accepting and building on each other\'s ideas. Learnt from Marc Rowland at Montreal Improv.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, team' \
        )
game.save!

game = player.games.new(:name => 'Meet & Greet', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => ' Everyone walks around meeting and greeting each other in different ways. For instance best friends, super heroes, suspicious neighbours, old school buddies, ex boyfriends, parents. Breaks the ice and gets people used to trying out different characters without thinking about it too much. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, ' \
        )
game.save!

game = player.games.new(:name => 'None, One, Many', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => 'Players begin a scene, but are not allowed to speak. They must perform this scene in silence, but they are NOT miming. Their silence must make sense in the scene. The moderator then calls out, "One." They can now say one word, but the idea is not to sound like a cave man. The single word must make sense in the context of the scene. The moderator would then call out "Many." Again, the idea is to make this new step make sense. The players don\'t just suddenly start speaking normally. They should work up to it, building on what has come before. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, discovery, scene building, character, story' \
        )
game.save!

#game = player.games.new(:name => 'Rhyme and Reason', \
#        :name_alt => '', \
#        :difficulty_id => '1', \
#        :type_id => '2', \
#        :players_min => '', \
#        :players_max => '', \
#        :description => '', \
#        :intro => '', \
#        :notes => '', \
#        :sample => '', \
#        :tag_list => ', ' \
#        )
#game.save!

game = player.games.new(:name => 'Pattern Game', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '8', \
        :description => 'First person names a word. The second person then associates with that word. Based on those two moves, the third person tries to establish a pattern. Once the pattern is "established" or "understood" by the whole group, then a new word is chosen. ', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'warmup, pattern' \
        )
game.save!

game = player.games.new(:name => 'Blind Scenes', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '2', \
        :players_max => '2', \
        :description => ' Player One starts engaged in the environment (with an action, object,  atmosphere, etc.) without words. Player Two starts with his back to the stage. Give  Player One time to engage in the activity/environment and make sure s/he feels about that  activity/ environment. With his back turned to what\'s happening on stage, Player Two  chooses a way to feel (without any context). When the Instructor says, "Go," Player Two  turns toward stage and speaks through the filter of his chosen emotion – he can react to  the scene partner OR he can react to his own activity/environment. ', \
        :intro => '', \
        :notes => 'Note: Player Two  needn\'t be literally entering the scene every time. He is turning into the scene for the  purposes of being kept unaware of Player One\'s physicality, but having turned he can  (and should) act as though he\'s been facing the scene the entire time. ', \
        :sample => '', \
        :tag_list => 'warmup, emotion, commitment' \
        )
game.save!

#game = player.games.new(:name => 'Secrets', \
#        :name_alt => '', \
#        :difficulty_id => '2', \
#        :type_id => '1', \
#        :players_min => '2', \
#        :players_max => '2', \
#        :description => 'Player One is given a secret objective that elicits an emotional response  from Player Two\'s character (Ex: get your partner to hug you, make your partner cry,  make your partner\'s character laugh, etc.). Player One is to pursue this objective  WITHOUT explicitly asking Player Two for what they want (Ex: "I\'m lonely," "I just  heard the saddest story," "Stupid chicken fucked a duck," etc.). ', \
#        :intro => '', \
#        :notes => 'Player One IS NOT ALLOWED to accomplish the objective within the 1st  minute of the scene (instructor will raise his/her hand at the 1 minute mark)  - The scene will end between the 3-5 minute mark (whenever is a good time to call  the scene), regardless of whether or not the objective was accomplished.  - Instructor asks the class what they think the objective was before revealing what it  was and whether Player 1 accomplished their objective. ', \
#        :sample => '', \
#        :tag_list => 'warmup, objective, wants, emotion' \
#        )
#game.save!

game = player.games.new(:name => 'Scene Stealing', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '1', \
        :players_min => '4', \
        :players_max => '4', \
        :description => 'Two players do a scene. Two different players redo the scene,  repeating and heightening details, characters, stakes, and emotion. ', \
        :intro => '', \
        :notes => 'Lessons:  - We remember the good stuff – they\'ll drop questions, carry over specifics, and  remember good stuff, point that out  - The bad stuff becomes good when we repeat it – make each other look good!  The first time is "random"; the second time is "purposeful"; the third time is  "expected."  - Don\'t skimp on the emotion - Player Two might have been simply  overwhelmed during the Offer dialogue, but Player Three and Four heighten the  emotion of being overwhelmed characters.  - Remember beginnings and ends – The first line and the last line of the previous  scene are the ones most guaranteed to, if repeated/heightened, connect with the  audience\'s pattern mind. ', \
        :sample => '', \
        :tag_list => 'warmup, heightening, stakes, emotion, details' \
        )
game.save!

