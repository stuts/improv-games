player = Player.find_by username: 'stuts'
PaperTrail.request.whodunnit = "1"

game = player.games.new(:name => 'That\'s What She Said', \
        :name_alt => '', \
        :difficulty_id => '1', \
        :type_id => '2', \
        :players_min => '3', \
        :players_max => '3', \
        :description => 'The players split into teams. One team starts doing a scene until another team shouts, "That\'s what she said!" and then that team starts a brand new scene using the last line from the last scene as the first line of their scene.', \
        :intro => '', \
        :notes => '', \
        :sample => '', \
        :tag_list => 'switch, justification, endowment, ' \
        )
game.save!
