class ContactsController < ApplicationController
  def new
    @title = "Contact Us"
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(params[:contact])
    @contact.request = request
    respond_to do |format| 
      if @contact.deliver 
        format.html { redirect_to root_path, success: 'Message sent successfully!' }
        format.json { render :show, status: :ok }
      else
        format.html { render :new, error: "Oh no, that wasn't supposed to happen" }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end
end
