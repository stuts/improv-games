class PlayersController < ApplicationController
  def show
    @player = Player.find_by_username(params[:username])
    sort = params[:sort]
    sort = 'name' unless sort.in?(['name', 'players_min', 'players_max'])
    # Locate all games for player where a version change has involved them
    @games = Game.joins(:versions).where(:versions => {:whodunnit => @player.id}).distinct.order(sort).paginate(:page => params[:page], :per_page => 5)
    @creations = @player.games.count
    @contributions = Game.joins(:versions).where(:versions => {:whodunnit => @player.id}).count
    @image = @player.avatar.attached? ? @player.avatar : "profile_image.png" 
    @title = @player.username
    @feed = get_activity(recent=9, player=@player)
  end

  def delete_avatar
    @player = current_player
    @image = ActiveStorage::Attachment.find(@player.avatar.id)
    @image.purge

    redirect_to show_player_path(@player.username)
  end

end
