class TagsController < ApplicationController
  def index
    @tags = ActsAsTaggableOn::Tag.all
    @toptags = ActsAsTaggableOn::Tag.most_used(10)
    @title = "Tags"
  end

  def show
    @tag =  ActsAsTaggableOn::Tag.find_by_name(params[:id])
    sort = params[:sort]
    sort = 'name' unless sort.in?(['name', 'players_min', 'players_max'])
    @games = Game.tagged_with(@tag.name).order(sort).paginate(:page => params[:page], :per_page => 10)
    @title = word_cap(@tag.name)
  end
end
