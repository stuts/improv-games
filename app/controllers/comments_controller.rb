class CommentsController < ApplicationController

  before_action :authenticate_player!, only: [:create, :destroy]

  def create
    @comment = current_player.comments.new(comment_params)
    @game = Game.find(@comment.game_id)
    respond_to do |format|
      if @comment.save
        format.html { redirect_to @game, success: 'Comment successfully created.' }
        format.json { render :show, status: :created, location: @game }
      else
        format.html { redirect_to @game, danger: 'Comment cannot be empty.' }
      end
    end
  end

  def comment_params
    params.require(:comment).permit(:content, :player_id, :game_id)
  end

  def destroy
    @comment = Comment.find(params[:id])
    @game = Game.find(@comment.game_id)
    @player = Player.find(@comment.player_id)

    @comment.delete

    redirect_to "/games/#{@game.slug}", :notice => "Comment deleted"
  end

end
