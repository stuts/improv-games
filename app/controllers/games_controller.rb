class GamesController < ApplicationController
  before_action :find_game, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_player!, except: [:index, :show]

  helper_method :get_video_id

  autocomplete :tag, :name, :class_name => "ActsAsTaggableOn::Tag"

  def show
    @title = word_cap(@game.name)
    @players = @game.versions.map { |v| Player.find(v.whodunnit) }.uniq
  end

  def new
    @title = "Add a Game"
    @game = current_player.games.build
    @submit_text = "Create game"
  end

  def edit
    @title = "Edit #{word_cap(@game.name)}"
    @submit_text = "Update game"
  end

  def destroy
    @game.destroy
    respond_to do |format|
      format.html { redirect_to games_path, success: 'Game was successfully deleted.' }
      format.json { render :show, status: :ok, location: @game }
    end
  end
  
  def update
    respond_to do |format|
      if @game.update(game_params)
        format.html { redirect_to @game, success: 'Game was successfully updated.' }
        format.json { render :show, status: :ok, location: @game }
      else
        format.html { render :edit }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @game = current_player.games.new(game_params)

    respond_to do |format|
      if @game.save
        format.html { redirect_to @game, success: 'Game was successfully created.' }
        format.json { render :show, status: :created, location: @game }
      else
        format.html { render :new }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  def index
    sort = params[:sort]
    sort = 'name' unless sort.in?(['name', 'players_min', 'players_max'])
    @games = Game.order(sort).paginate(:page => params[:page], :per_page => 20)
    @title = "Games"
  end

  private

  # Never trust internet parameters, whitelist through here
  def game_params
    params.require(:game).permit(:name, :name_alt, :players_min, :players_max, :description, :intro, :notes, :sample, :tag_list, :difficulty_id, :type_id)
  end

  # Locate game by slug
  def find_game
    @game = Game.find_by slug: params[:slug]
    @difficulty = Difficulty.find(@game.difficulty_id)
    ! @game.type_id.nil? ? @type = Type.find(@game.type_id) : @type = ''
  end

  # Strip the video ID from YouTube URLs, compatible with both standard and share URLs
  def get_video_id(url)
    return url.split(/[\/,\=]/).last
  end
end
