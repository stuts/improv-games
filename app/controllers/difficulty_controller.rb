class DifficultyController < ApplicationController
  before_action :get_difficulty, only: [:show]

  def show
    sort = params[:sort]
    sort = 'name' unless sort.in?(['name', 'players_min', 'players_max'])
    @games = Game.where(:difficulty_id => @difficulty.id).order(sort).paginate(:page => params[:page], :per_page => 20)
    @title = word_cap(@difficulty.name)
  end

  def index
    @difficulties = Difficulty.all
    @title = "Difficulties"
  end

  private
  def get_difficulty
    @difficulty = Difficulty.find_by name: params[:name]
  end
end
