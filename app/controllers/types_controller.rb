class TypesController < ApplicationController
  before_action :get_type, only: [:show]

  def show
    sort = params[:sort]
    sort = 'name' unless sort.in?(['name', 'players_min', 'players_max'])
    @games = Game.where(:type_id => @type.id).order(sort).paginate(:page => params[:page], :per_page => 20)
    @title = word_cap(@type.name)
  end

  def index
    @types = Type.all
    @title = "Types"
  end

  private
  def get_type
    @type = Type.find_by name: params[:name]
    puts @type.inspect
  end
end
