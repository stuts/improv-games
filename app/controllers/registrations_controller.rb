class RegistrationsController < Devise::RegistrationsController

  protected

  def after_update_path_for(resource)
    show_player_url(resource.username)
  end

  def update_resource(resource, params)
    resource.update_without_password(params)
  end
  
  private

  def sign_up_params
    params.require(:player).permit(:username, :email, :password, :password_confirmation)
  end

  def account_update_params
    params.require(:player).permit(:display_name, :website, :bio, :username, :email, :password, :password_confirmation, :current_password, :avatar, :facebook, :twitter, :instagram, :youtube)
  end

end
