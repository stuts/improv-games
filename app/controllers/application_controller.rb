class ApplicationController < ActionController::Base
  # Flash
  add_flash_types :success, :warning, :danger, :info

  # Stuff for doing username authentication
  before_action :configure_permitted_parameters, if: :devise_controller?
  protected
  def configure_permitted_parameters
   devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :email, :password, :password_confirmation])
   devise_parameter_sanitizer.permit(:sign_in, keys: [:login, :password, :password_confirmation])
   devise_parameter_sanitizer.permit(:account_update, keys: [:username, :email, :password, :password_confirmation, :current_password])
  end

  # Log who made the changes
  alias_method :current_user, :current_player # because paper_trail looks for current_user... Perhaps renaming users was a bad idea
  before_action :set_paper_trail_whodunnit

  # Helper methods
  helper_method :word_cap
  helper_method :socialicon
  helper_method :sociallink
  helper_method :find_difficulty
  helper_method :get_activity

  def word_cap(string)
    # Capitalise every word in string (keeping punctuation but properly capitalising things within)
    return string.humanize.gsub(/\b('?[a-z])/) { $1.capitalize }
  end

  def sociallink(site, link)
    # Define link hashes
    links = {
      'facebook' => {
        url: 'https://facebook.com'
      },
      'twitter' => {
        url: 'https://twitter.com'
      },
      'instagram' => {
        url: 'https://instagram.com'
      },
      'youtube' => {
        url: 'https://youtube.com/user'
      },
      'website' => {
        url: ''
      }
    }

    # Validate links
    if site == 'website'
      if link.include?('http')
        url = link
      else
        url = "https://#{link}"
      end
      return "<p><a target='_blank' rel='noopener noreferrer' class='socialicon fa fa-lg fa-globe' href='#{url}'><a> <a target='_blank' rel='noopener noreferrer' href='#{url}'>#{link}</a></p>".html_safe
    else
      url = "#{links[site][:url]}/#{link}"
      return "<p><a target='_blank' rel='noopener noreferrer' class='socialicon fab fa-lg fa-#{site}' href='#{url}'></a> <a target='_blank' rel='noopener noreferrer' href='#{url}'>#{link}</a></p>".html_safe
    end
  end

  def socialicon(site, title='', tags='', caption='')
    # Define icon hashes
    icons = {
      'facebook' => {
        shareurl: 'https://www.facebook.com/sharer.php?u=LINK'
      },
      'twitter' => { 
        shareurl: 'https://twitter.com/intent/tweet?url=LINK&text=TITLE&hashtags=TAGS'
      },
      'tumblr' => {
        shareurl: 'https://www.tumblr.com/widgets/share/tool?canonicalUrl=LINK&title=TITLE&caption=CAPTION&tags=TAGS'
      },
      'reddit' => { 
        shareurl: 'https://reddit.com/submit?url=LINK&title=TITLE'
      },
      'pinterest' => {
        shareurl: 'http://pinterest.com/pin/create/link/?url=LINK'
      },
      'envelope' => {
        shareurl: 'mailto:?subject=TITLE&body=LINK'
      }
    }
    
    # Do the link generation stuff
    socialsite = icons[site]
    socialsite[:name] = site
    socialsite[:href] = URI.encode(socialsite[:shareurl].sub('LINK', request.original_url).sub('TITLE', title).sub('TAGS', tags).sub('CAPTION', caption))
    return socialsite
  end

  def find_difficulty(difficulty_id)
    return Difficulty.find(difficulty_id)
  end

  def find_type(type_id)
    return Type.find(type_id)
  end

  def get_activity(recent=9, player=nil)
    if player
      versions = PaperTrail::Version.all.where(:whodunnit => player.id)
      comments = Comment.all.where(:player_id => player.id)
    else
      versions = PaperTrail::Version.all
      comments = Comment.all
    end
    feed = versions + comments
    if feed.count < recent
      sort = feed.sort_by(&:created_at).reverse[0..feed.count]
    else
      sort = feed.sort_by(&:created_at).reverse[0..recent]
    end
    # Make keys consistent
    mappings = {"item_id" => "game_id", "whodunnit" => "player_id", "object_changes" => "content"}
    # Tidy content for sharing
    sort = sort.as_json
    sort.each do |item|
      item.keys.each { |k| item[ mappings[k] ] = item.delete(k) if mappings[k]}
    end
    return sort
  end
end
