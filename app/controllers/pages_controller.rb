class PagesController < ApplicationController

  def show
    # For search page NOTE: I KNOW THIS IS PROBABLY A SHIT WAY TO IMPLEMENT
    if params[:search]
      @results = PgSearch.multisearch(params[:search])
    end

    if params[:page] == 'home'
      @game_count = Game.count
      @tag_count = ActsAsTaggableOn::Tag.count
      @player_count = Player.where.not(:confirmed_at => nil).count
      @feed = get_activity
    end

    if params[:page] == 'feed'
      @feed = get_activity(recent=49)
    end

    @title = word_cap(params[:page])

    # Render page
    render template: "pages/#{params[:page]}"
  end

end
