// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery
//= require jquery_ujs
//= require jquery-ui/widgets/autocomplete
//= require autocomplete-rails
//= require bootstrap
//= require activestorage
//= require turbolinks
//= require trix
//= require_tree .

// Popup window code
function newPopup(url) {
    popupWindow = window.open(
        url,'popUpWindow','height=300,width=400,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}

// Allow linking to tab headings
function tabLinks() {
    // Javascript to enable link to tab
    var hash = document.location.hash;
    var prevHash = document.location.hash;
    //console.log('Start of tabLinks, hash is ' + hash)
    //console.log('Set previous hash to ' + prevHash)
    var previousUrl = document.location.href.replace(location.hash,"");
    if (hash) {
        $('.nav-tabs a[href=\\'+hash+']').tab('show');
    }

    // Change hash for page-reload
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        window.location.hash = e.target.hash;
        prevHash = window.location.hash;
        //console.log('Set previous hash to ' + prevHash + ' (data toggle)')
    });

    // Handle back button stuff
    $(window).on('popstate', function() {
        currentUrl = document.location.href.replace(location.hash,"");
        currentHash = document.location.hash;
        if (previousUrl !== currentUrl) {
            //console.log('Current URL (' + currentUrl + ') is different from previous (' + previousUrl + ')');
            location.reload(true);
        } else if (prevHash !== currentHash) {
            //console.log('Tab has changed from ' + prevHash + ' to ' + currentHash);
            if (currentHash) {
                $('.nav-tabs a[href=\\'+currentHash+']').tab('show');
            }
        } else {
            //console.log('Everything is the same, doing nothing (' + previousUrl + ' ' + currentUrl + ' ' + hash + ' ' + currentHash + ')');
        }
    });
 }
