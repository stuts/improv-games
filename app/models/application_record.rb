class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  # Method for converting a string into a slug
  def to_slug(string)
    string.parameterize.truncate(80, omission: '')
  end

  # Return capitalised name
  def name_in_caps
    name.titleize
  end
end
