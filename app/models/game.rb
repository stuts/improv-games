class Game < ApplicationRecord
  # Version logging/auditing/changelog
  has_paper_trail

  # Tagging
  acts_as_taggable

  # Search 
  include PgSearch
  multisearchable :against => [:name, :name_alt, :description, :intro, :notes]

  # Relationships
  belongs_to :player

  # Pre-Save Activities
  before_save { self.slug = self.to_slug(self.name) }

  # Validation
  validates_uniqueness_of :name
  validates_uniqueness_of :slug
  validates :name, presence: true
  validates :players_min, presence: true
  validates :players_max, presence: true
  validates :description, presence: true
#  validates :difficulty, presence: true
  validate :players_range

  def to_param
    slug
  end

  private

  def players_range
    unless players_min.nil? || players_max.nil?
      if players_min > players_max
        errors.add(:players_min, "cannot be larger than players max")
      end
      if players_min < 1
        errors.add(:players_min, "cannot be below 1")
      end
      if players_max < 1
        errors.add(:players_max, "cannot be below 1")
      end
    end
  end
end
