class Comment < ApplicationRecord
  # Relationships
  belongs_to :player

  # Validation
  validates :content, presence: true
end
