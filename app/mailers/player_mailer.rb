class PlayerMailer < ApplicationMailer
  default from: "stu@improvgames.fun"

  def sample_email(player)
    @player = player
    mail(to: @player.email, subject: 'Sample Email')
  end
end
