class AddTypeIdToGames < ActiveRecord::Migration[5.2]
  def change
    add_column :games, :type_id, :integer
  end
end
