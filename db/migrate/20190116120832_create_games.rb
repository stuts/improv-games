class CreateGames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.string :name
      t.text :name_alt
      t.integer :players_min
      t.integer :players_max
      t.text :description
      t.text :intro
      t.text :notes
      t.string :sample

      t.timestamps
    end
  end
end
