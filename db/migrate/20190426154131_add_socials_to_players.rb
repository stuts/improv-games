class AddSocialsToPlayers < ActiveRecord::Migration[5.2]
  def change
    add_column :players, :facebook, :text
    add_column :players, :twitter, :text
    add_column :players, :instagram, :text
    add_column :players, :youtube, :text
  end
end
