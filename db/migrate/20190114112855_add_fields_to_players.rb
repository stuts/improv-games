class AddFieldsToPlayers < ActiveRecord::Migration[5.2]
  def change
    add_column :players, :display_name, :text
    add_column :players, :website, :text
    add_column :players, :bio, :text
  end
end
