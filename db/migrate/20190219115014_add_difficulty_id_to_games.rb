class AddDifficultyIdToGames < ActiveRecord::Migration[5.2]
  def change
    add_column :games, :difficulty_id, :integer
  end
end
