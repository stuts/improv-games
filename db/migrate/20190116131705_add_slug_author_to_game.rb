class AddSlugAuthorToGame < ActiveRecord::Migration[5.2]
  def change
    add_column :games, :slug, :string
    add_column :games, :author, :string
  end
end
