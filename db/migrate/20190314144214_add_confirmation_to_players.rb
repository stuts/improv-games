class AddConfirmationToPlayers < ActiveRecord::Migration[5.2]
  def change
    add_column :players, :confirmation_token, :string
    add_column :players, :confirmed_at, :datetime
    add_column :players, :confirmation_sent_at, :datetime
    add_column :players, :unconfirmed_email, :string
  end
end
