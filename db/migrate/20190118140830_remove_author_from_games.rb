class RemoveAuthorFromGames < ActiveRecord::Migration[5.2]
  def change
    remove_column :games, :author
  end
end
