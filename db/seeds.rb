# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Create Difficulties



# Create Types
types = Type.create!([{
  id: 1,
  name: 'warmup',
  description: 'games to get the group mind flowing'
  },
  {
  id: 2,
  name: 'exercise',
  description: 'game to help work on those key skills'
  },
  {
  id: 3,
  name: 'show',
  description: 'games to put on the stage'
  }])
